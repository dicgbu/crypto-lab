# Chapter 1

## Introduction 
Python has two running major versions - Python 2 and Python 3. 
We recommend Python 3 version to use. Here we used Python 3.

## Python Basics
> ## Comments on Installation
> - Choose Python 3
> - It is much better to install Anaconda-3. It installs additionsl feature to python. It includes "Spyder", an IDE to edit and compile python code. It also includes a web based interactive interface "Jupyter". We mostly use Jupyter.

Now it is the time to check the installation:
You may run following first code. 

```python
 print("Welcome Crypto Lab")
```

## Python keywords and identifiers
Python keywords are the words that are reserved. We can’t use them as name of any entities like variables, classes and functions. These are for defining the syntax and structures of Python language.

You should know there are 33 keywords in Python. However, in coming versions these may be less or more. 

 Keywords in Python are case sensitive. Here is a list of all keywords in python programming.

 ```python
 
>>> help()

Welcome to Python 3.6's help utility!

help> keywords

Here is a list of the Python keywords.  Enter any keyword to get more help.

False               def                 if                  raise
None                del                 import              return
True                elif                in                  try
and                 else                is                  while
as                  except              lambda              with
assert              finally             nonlocal            yield
break               for                 not                 
class               from                or                  
continue            global              pass                

help> 
```