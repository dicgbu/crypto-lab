# Number Theory

## Introduction 
In this chapter we will learn some number theoretic algorithms in python.

## Exponential Squaring (Fast Modulo Multiplication)

### Binary Method
```python
# Computes exponential value under modulo using binary exponentiation. 
# prime modulo value 
N = 1000000007; 
	
# Function code 
def exponentiation(base, exponent): 
	if (exponent == 0): 
		return 1; 
	if (exponent == 1): 
		return base % N; 
	
	t = exponentiation(base, int(exponent / 2)); 
	t = (t * t) % N; 
	if (exp % 2 == 0):                  # if exponent is even
		return t; 
	else:                               # if exponent is odd 
		return ((base % N) * t) % N; 

# Driver code 
base = 5; 
exp = 100000; 
res = exponentiation(base, exp); 

print(res); 

```

### $2^k$-ary method:

```python
# Python3 program to compute exponential value using (2^k)-ary method. 
# prime modulo value 
N = 1000000007; 

def exponentiation(base, exponent): 
	t = 1; 
	while(exponent > 0): 
		if (exponent % 2 != 0):           # When Exponent is odd 
			t = (t * base) % N; 

		base = (base* base) % N; 
		exponent = int(exponent / 2); 
	return t % N; 

# Driver Code 
base = 5; 
exp = 100000; 
res = exponentiation(base,exp); 
print(res); 

